package com.ejbank.user;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class User {
	private int id;
	private String login;
	private String password;
	private String email;
	private String firstname;
	private String lastname;
	private String type;
}
