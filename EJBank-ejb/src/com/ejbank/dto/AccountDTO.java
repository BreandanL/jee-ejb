package com.ejbank.dto;

public class AccountDTO {
	private int id;
	private String type;
	private long amount;

	public int getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public long getAmount() {
		return amount;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setType(String type) {
		this.type = type;	
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}
}
