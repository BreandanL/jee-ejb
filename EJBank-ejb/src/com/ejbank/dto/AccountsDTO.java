package com.ejbank.dto;

import java.util.List;

public class AccountsDTO {
	private List<AccountDTO> accounts;
	private String error;

	public AccountsDTO(List<AccountDTO> accountPayloads) {
		this.accounts = accountPayloads;
	}

	public List<AccountDTO> getAccounts() {
		return accounts;
	}

	public String getError() {
		return error;
	}
}
