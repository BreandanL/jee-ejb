package com.ejbank.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ejbank_advisor")
@DiscriminatorValue(value = "advisor")
public class EntityAdvisor extends EntityUser implements Serializable {

	private static final long serialVersionUID = 1L;
	@OneToMany
	@JoinColumn(name = "advisor_id", referencedColumnName = "id")
	private List<EntityCustomer> customers;

}
