package com.ejbank.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ejbank_transaction")
public class EntityTransaction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name = "author", nullable = false, length = 11)
	private int author;

	@Column(name = "amount", nullable = false, length = 10)
	private long amount;

	@Column(name = "comment", nullable = false)
	private String comment;

	@Column(name = "applied", nullable = false, length = 1)
	private boolean applied;
}
