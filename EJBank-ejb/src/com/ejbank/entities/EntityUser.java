package com.ejbank.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ejbank_user")
@DiscriminatorColumn(name = "type")
public abstract class EntityUser implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "login", nullable = false, length = 8)
	private String login;

	@Column(name = "password", nullable = false)
	private String password;

	@Column(name = "email", nullable = false)
	private String email;

	@Column(name = "firstname", nullable = false, length = 50)
	private String firstame;

	@Column(name = "lastname", nullable = false, length = 50)
	private String lastname;

	@Column(name = "type", nullable = false, length = 50)
	private String type;
}
