package com.ejbank.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ejbank_account")
public class EntityAccount implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "balance", length = 10)
	private long balance;

	@OneToOne
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private EntityUser foreignUser;

	@ManyToOne
	@JoinColumn(name = "account_type_id", referencedColumnName = "id")
	private EntityAccountType foreignAccountType;

	public EntityAccountType getForeignAccountType() {
		return foreignAccountType;
		
	}

	public int getId() {
		return id;
	}

	public EntityAccountType getAccountType() {
		return foreignAccountType;
	}

	public long getBalance() {
		return balance;
	}
}
