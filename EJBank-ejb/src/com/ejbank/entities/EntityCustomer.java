package com.ejbank.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ejbank_customer")
@DiscriminatorValue("customer")
@NamedQueries({
		@NamedQuery(name = "findCustomerByLogin", query = "SELECT u FROM EjbankCustomer u WHERE u.login = login") })
public class EntityCustomer extends EntityUser {
	private static final long serialVersionUID = 1L;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
	private List<EntityAccount> accountList;

	@JoinColumn(name = "advisor_id", referencedColumnName = "id", nullable = false)
	private EntityAdvisor advisor;

	public List<EntityAccount> getAccountList() {
		return accountList;
	}

	public EntityAdvisor getAdvisor() {
		return advisor;
	}

	public List<EntityAccount> getAccounts() {
		return accountList;
	}
}
