package com.ejbank.sessions;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.ejbank.entities.EntityAccount;
import com.ejbank.entities.EntityCustomer;

public class BeanAccountLocal implements BeanAccount {

	@PersistenceContext(unitName = "EJBankPU")
    private EntityManager em;
	
	@Override
	public List<EntityAccount> getAccounts(int userId) {
		EntityCustomer customer = em.find(EntityCustomer.class, userId);
		for (EntityAccount account : customer.getAccounts()) {
			account.getForeignAccountType();
		}
		return customer.getAccounts();
	}

}
