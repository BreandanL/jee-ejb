package com.ejbank.sessions;

import java.util.List;

import com.ejbank.entities.EntityAccount;

public interface BeanAccount {
	List<EntityAccount> getAccounts(int userId);
}
