package com.ejbank.api;

import com.ejbank.sessions.BeanAccount;
import com.ejbank.entities.EntityAccount;
import com.ejbank.dto.AccountDTO;
import com.ejbank.dto.AccountsDTO;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class AccountService {

	@EJB
	private BeanAccount beanAccount;

	@GET
	@Path("/{user_id}")
	@Produces("application/json")
	public AccountsDTO getUserEJB(@PathParam("user_id") Integer userId) {
		Objects.requireNonNull(userId);
		List<EntityAccount> accounts = beanAccount.getAccounts(userId);
		List<AccountDTO> accountDTOs = new ArrayList<>();
		for (EntityAccount account : accounts) {
			AccountDTO accountDTO = new AccountDTO();
			accountDTO.setId(account.getId());
			accountDTO.setType(account.getAccountType().getName());
			accountDTO.setAmount(account.getBalance());
			accountDTOs.add(accountDTO);
		}

		return new AccountsDTO(accountDTOs);
	}

	@GET
	@Path("/attached/{user_id}")
	@Produces("application/json")
	public AccountsDTO getAccountsAttached(@PathParam("user_id") Integer userId) {
		Objects.requireNonNull(userId);
		List<EntityAccount> accounts = beanAccount.getAccounts(userId);
		List<AccountDTO> accountDTOs = new ArrayList<>();
		for (EntityAccount account : accounts) {
			AccountDTO accountDTO = new AccountDTO();
			accountDTO.setId(account.getId());
			accountDTO.setType(account.getAccountType().getName());
			accountDTO.setAmount(account.getBalance());
			accountDTOs.add(accountDTO);
		}

		return new AccountsDTO(accountDTOs);
	}
}
